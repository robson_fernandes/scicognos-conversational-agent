from genericpath import exists
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


from googletrans import Translator
from transformers import AutoModelForCausalLM, AutoTokenizer
import torch
import pickle

app = FastAPI()


app.add_middleware(CORSMiddleware, 
            allow_origins=['*'],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
            )


@app.get("/api/v1/scicognos/conversational/agent")
def conversational_agent(message: str = '', lang: str = 'en', step: int = 0):

    tokenizer = AutoTokenizer.from_pretrained("microsoft/DialoGPT-large")
    model = AutoModelForCausalLM.from_pretrained("microsoft/DialoGPT-large")
    
    translator = Translator()

    output_lang = lang
        
    talk = message #input(">> User:")
    text = translator.translate(talk, dest='en')
    
    if exists("history.pkl"):
        chat_history_ids = pickle.load(open("history.pkl",'rb'))

    # encode the new user input, add the eos_token and return a tensor in Pytorch
    new_user_input_ids = tokenizer.encode(text.text + tokenizer.eos_token, return_tensors='pt')
 
    # append the new user input tokens to the chat history
    bot_input_ids = torch.cat([chat_history_ids, new_user_input_ids], dim=-1) if step > 0 else new_user_input_ids

    # generated a response while limiting the total chat history to 1000 tokens, 
    chat_history_ids = model.generate(bot_input_ids, max_length=1000, pad_token_id=tokenizer.eos_token_id)

    if not exists("history.pkl"):
        pickle.dump(chat_history_ids, open("history.pkl", "wb"))


    response = tokenizer.decode(chat_history_ids[:, bot_input_ids.shape[-1]:][0], skip_special_tokens=True)
    
    response = translator.translate(response, dest=output_lang).text

    return {"response": response}



if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0")